#[path="raw_socket.rs"]
mod raw_socket;
use raw_socket::RawSocket1;

#[path="filter.rs"]
mod filter;

use std::thread;
use libc::{ETH_P_IP,ETH_P_IPV6};

fn gen_hop(defaultTTL:u8,resTTL:u8,delta:u8)->u8{
	let t1=resTTL as i16;
	let list1:[i16;3]=[64,128,255];
	let mut maybe=0i16;
	for e in list1{
		maybe = e - t1;
		if maybe>=0{
			break
		}
	}
	maybe += 1 - (delta as i16);
	if maybe>=0 && (maybe as u8)<defaultTTL{
		maybe as u8
	}else{
		defaultTTL
	}
}
fn test1(protocol:i32, defaultTTL:u8, guessHop:bool, delta:u8){
	let fn1:Box<dyn Fn(u8,u8)->u8>;
	if guessHop{
		fn1=Box::new(|a,res|{
			gen_hop(a,res,delta)
		});
	}else{
		fn1=Box::new(|a,res|{a});
	}
	let sock=RawSocket1::new(protocol).unwrap();
	let mut buf=[0u8;65536];
	loop{
		match sock.recv_from(&mut buf[..],0){
			Ok((rLen,addr))=>{
				filter::filter(
					&buf[..rLen],
					&addr,
					defaultTTL,
					&fn1,
					&sock,
					RawSocket1::send_to);
				()
			},
			Err(e)=>{
				eprintln!("{:#?}",e);
				break
			}
		}
	}
}

fn main(){
	let arg:Vec<String>=std::env::args().collect();
	let TTL1=if let Some(s)=arg.get(1){
		u8::from_str_radix(s,10).unwrap()
	}else{
		10
	};
	let guessHop=if let Some(s)=arg.get(2){
		if &*s == "true"{
			true
		}else{
			false
		}
	}else{
		false
	};
	let delta=if let Some(s)=arg.get(3){
		u8::from_str_radix(s,10).unwrap()
	}else{
		1
	};
	println!("default TTL: {}\nguess hop: {}\ndelta: {}",TTL1,guessHop,delta);
	thread::spawn(move ||{
		test1(ETH_P_IPV6,TTL1,guessHop,delta);
	});
	test1(ETH_P_IP,TTL1,guessHop,delta);
}
