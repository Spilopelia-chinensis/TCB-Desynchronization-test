use libc::{AF_PACKET,SOCK_DGRAM};
use std::io;
use std::mem;
use std::fmt;

#[repr(C)]
pub struct SockAddrLL{
	pub sll_family:u16,   /* Always AF_PACKET */
	pub sll_protocol:u16, /* Physical-layer protocol */
	pub sll_ifindex:i32,  /* Interface number */
	pub sll_hatype:u16,   /* ARP hardware type */
	pub sll_pkttype:u8,   /* Packet type */
	pub sll_halen:u8,     /* Length of address */
	pub sll_addr:[u8;8]   /* Physical-layer address */
}
pub struct RawSocket1{
	pub fd:i32
}
pub fn u16_hton(n:u16)->u16{
	u16::from_be_bytes(n.to_ne_bytes())
}
pub fn slice_to_hex_string(s:&[u8])->String{
	let mut t:Vec<String>=Vec::with_capacity(s.len());
	for e in s{
		t.push(format!("{:0>2}",format!("{:x}",e)));
	}
	t.join(" ")
}
impl fmt::Debug for SockAddrLL{
	fn fmt(&self, fmt:&mut fmt::Formatter<'_>)->fmt::Result{
		fmt.debug_struct("SockAddrLL")
			.field("sll_family",&self.sll_family)
			.field("sll_protocol",&self.sll_protocol)
			.field("sll_ifindex",&self.sll_ifindex)
			.field("sll_hatype",&self.sll_hatype)
			.field("sll_pkttype",&self.sll_pkttype)
			.field("sll_halen",&self.sll_halen)
			.field("sll_addr",&slice_to_hex_string(&self.sll_addr[..]))
			.finish()
	}
}
impl SockAddrLL{
	pub fn new()->SockAddrLL{
		SockAddrLL{
			sll_family:0,
			sll_protocol:0,
			sll_ifindex:0,
			sll_hatype:0,
			sll_pkttype:0,
			sll_halen:0,
			sll_addr:[0;8]
		}
	}
}
impl Drop for RawSocket1{
	fn drop(&mut self){
		unsafe{ libc::close(self.fd); }
	}
}
impl RawSocket1{
	pub fn new(protocol:i32)->Result<RawSocket1,io::Error>{
		let fd=unsafe{ libc::socket(AF_PACKET,SOCK_DGRAM,u16_hton(protocol as u16) as i32) };
		if fd!=-1{
			Ok(RawSocket1{
				fd:fd
			})
		}else{
			Err(io::Error::last_os_error())
		}
	}
	pub fn recv_from(&self, buf:&mut [u8], flags:i32)->Result<(usize,SockAddrLL),io::Error>{
		let mut addr1=SockAddrLL::new();
		let mut addrLen=mem::size_of::<SockAddrLL>() as u32;
		let p1:*mut SockAddrLL=&mut addr1;
		let rLen=unsafe{ libc::recvfrom(
			self.fd,
			buf.as_mut_ptr() as _,
			buf.len(),
			flags,
			p1 as _,
			&mut addrLen
		) };
		if rLen!=-1{
			Ok((rLen as usize,addr1))
		}else{
			Err(io::Error::last_os_error())
		}
	}
	pub fn send_to(&self, buf:&[u8], flags:i32, addr:&SockAddrLL)->Result<usize,io::Error>{
		let p1:*const SockAddrLL=addr;
		let sLen=unsafe { libc::sendto(
			self.fd,
			buf.as_ptr() as _,
			buf.len(),
			flags,
			p1 as _,
			mem::size_of::<SockAddrLL>() as u32
		) };
		if sLen!=-1{
			Ok(sLen as usize)
		}else{
			Err(io::Error::last_os_error())
		}
	}
}
